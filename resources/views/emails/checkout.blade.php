<p>Name: {{ $contactData['name'] }}</p>
<p>Contact details: {{ $contactData['contactDetails'] }}</p>
<p>Comments: {{ $contactData['comments'] }}</p>
<br><br>
<p>Products</p>
<br>
@foreach($products as $key => $value)
    @if(count($value) > 0)
        <p>Title: {{ $value[0]['title'] }}</p>
        <p>Description: {{ $value[0]['description'] }}</p>
        <p>Quantity: {{ $value['quantity'] }}</p>
        <p>Price per unit: {{ $value[0]['price'] }}</p>
        <br><br>
    @endif
@endforeach