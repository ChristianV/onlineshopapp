@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <h3>Shopping Cart</h3>
        <hr>
        <div class="row justify-content-center">
            @if(count($products) > 0)
                @foreach($products as $key => $value)
                    @if(count($value) > 0)
                        <div class="col-md-3">
                            <div class="card">
                                <div class="card-header">
                                    {{ $value[0]['title'] }}
                                </div>

                                <div class="card-body">
                                    <img src="{{ $value[0]['image'] }}" height="100" width="100">
                                    <br>
                                    {{ $value[0]['description'] }}
                                    <hr>
                                    Quantity: {{ $value['quantity'] }}
                                    <hr>
                                    Price per unit: {{ $value[0]['price'] }}
                                    @guest
                                        <br>
                                        <form action="{{ route('cart.remove', $value[0]['id']) }}" method="POST" id="removeFromCart_{{ $value[0]['id'] }}">
                                            @csrf
                                            <input type="submit" class="btn btn-danger" value="Remove" form="removeFromCart_{{ $value[0]['id'] }}">
                                        </form>
                                    @endguest
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach

                <h1>Checkout</h1>
                <div class="card-body">
                    <form method="POST" action="{{ route('cart.checkout') }}">
                        @csrf
                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <div class="form-row">
                                <label for="name">Name*</label>
                                <input class="form-control" id="name" name="name" type="text" placeholder="Enter name" value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('contactDetails') ? ' has-error' : '' }}">
                            <div class="form-row">
                                <label for="name">Contact details*</label>
                                <textarea class="form-control" id="contactDetails" name="contactDetails" placeholder="Enter contact details">{{ old('contactDetails') }}</textarea>
                                @if ($errors->has('contactDetails'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('contactDetails') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('comments') ? ' has-error' : '' }}">
                            <div class="form-row">
                                <label for="name">Comments</label>
                                <input class="form-control" id="comments" name="comments" type="text" placeholder="Enter comments" value="{{ old('comments') }}">
                                @if ($errors->has('comments'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('comments') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <button class="btn btn-info pull-right" type="submit">Checkout</button>
                    </form>
                </div>
            @else
                No products found.
            @endif
        </div>
    </div>
@endsection
