@extends('layouts.app')

@section('content')
    <div class="container">
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif
        <h3>Products list ({{ $products->total() }})</h3>
        @auth
            <button class="btn btn-dark" onclick="window.location='{{ route('products.create') }}'">Add product</button>
        @endauth
        <hr>
        <div class="row justify-content-center">
            @if($products->count() > 0)
                @foreach($products as $product)
                    <div class="col-md-3">
                        <div class="card">
                            <div class="card-header">
                                {{ $product->title }}
                                @auth
                                    <br>
                                    <button class="btn btn-info" onclick="window.location='{{ route('products.edit', $product->id) }}'">Edit</button>
                                    <form action="{{ route('products.destroy', $product->id) }}" method="POST" id="deleteForm">
                                        @csrf
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="submit" class="btn btn-danger" value="Delete" form="deleteForm">
                                    </form>
                                @endauth
                            </div>

                            <div class="card-body">
                                <img src="{{ $product->image }}" height="100" width="100">
                                <br>
                                {{ $product->description }}
                                <hr>
                                Price: {{ $product->price }}
                                @guest
                                    <br>
                                    <form action="{{ route('cart.add', $product->id) }}" method="POST" id="addToCart_{{ $product->id }}">
                                        @csrf
                                        <input type="submit" class="btn btn-danger" value="Add to cart" form="addToCart_{{ $product->id }}">
                                    </form>
                                @endguest
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                No products found.
            @endif
        </div>
        <hr>
        <div class="row justify-content-center">
            {{ $products->links() }}
        </div>
    </div>
@endsection
