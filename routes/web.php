<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('products');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

Route::get('products', 'ProductsController@index')->name('products.index');
Route::get('cart', 'CartController@index')->name('cart.index');
Route::post('cart/add/{product}', 'CartController@add')->name('cart.add');
Route::post('cart/remove/{product}', 'CartController@remove')->name('cart.remove');
Route::post('cart/checkout', 'CartController@checkout')->name('cart.checkout');

Route::middleware('auth:web')->group(function () {
    Route::get('admin', function() {
        return view('admin');
    });

    Route::get('products/create', 'ProductsController@create')->name('products.create');
    Route::get('products/edit/{product}', 'ProductsController@edit')->name('products.edit');
    Route::post('products', 'ProductsController@store')->name('products.store');
    Route::put('products/{product}', 'ProductsController@update')->name('products.update');
    Route::delete('products/{product}', 'ProductsController@destroy')->name('products.destroy');
});
