<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('users')
            ->where('id', '>', 0)
            ->delete();
        DB::table('users')
            ->insert([
                'name' => 'Admin',
                'username' => 'admin',
                'email' => 'admin@admin.ro',
                'password' => bcrypt('secret'),
            ]);
    }
}