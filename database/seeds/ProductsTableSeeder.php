<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProductsTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        DB::table('products')
            ->where('id', '>', 0)
            ->delete();
        DB::table('products')
            ->insert([
                'title' => 'Test product title',
                'description' => 'Test product description',
                'price' => 100,
                'image' => 'http://via.placeholder.com/100x100'
            ]);
    }
}