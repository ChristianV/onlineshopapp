<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class CheckoutOrder extends Mailable {
    use Queueable, SerializesModels;

    protected $products, $contactData;
    /**
     * Create a new message instance.
     * @param $products
     * @param $contactData
     *
     */
    public function __construct($products, $contactData) {
        $this->products = $products;
        $this->contactData = $contactData;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        return $this->from(config('shopVariables.mailFrom'))
            ->view('emails.checkout')
            ->with([
                'products' => $this->products,
                'contactData' => $this->contactData,
            ]);
    }
}
