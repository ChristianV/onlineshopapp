<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductsController extends Controller {
    /**
     * Products list
     */
    public function index() {
        $products = Product::orderBy("created_at", "desc")->paginate(config("shopVariables.itemsPerPage"));

        return view("products.index")
            ->with([
                "products" => $products
            ]);
    }

    /**
     * Create view
     * @return view
     */
    public function create() {
        return view("products.create");
    }

    /**
     * Creates a product
     * @param Request $request
     * @return redirect
     */
    public function store(Request $request) {
        $validator = Validator::make($request->all(), [
            "title" => "required|max:55",
            "description" => "required|max:255",
            "price" => "required",
            "image" => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ]);

        if($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

        $imgName = $this->uploadImage($request->file('image'));

        $product = new Product();
        $product->title = $request->input("title");
        $product->description = $request->input("description");
        $product->price = $request->input("price");
        $product->image = url('/images/' . $imgName);
        $product->save();

        return redirect("products")->with([
            "status" => "Product was added successfully."
        ]);
    }

    /**
     * Edit view
     * @param Product $product
     * @return view
     */
    public function edit(Product $product) {
        return view("products.edit")->with([
            "product" => $product
        ]);
    }

    /**
     * Updates the product
     * @param Request $request
     * @param Product $product
     * @return redirect
     */
    public function update(Request $request, Product $product) {
        $validator = Validator::make($request->all(), [
            "title" => "required|max:55",
            "description" => "required|max:255",
            "price" => "required",
            "image" => 'image|mimes:jpeg,png,jpg,gif,svg|max:5000'
        ]);

        if($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

        if($request->has('image')) {
            $imgName = $this->uploadImage($request->file('image'));
            $product->image = url('/images/' . $imgName);
        }

        $product->title = $request->input("title");
        $product->description = $request->input("description");
        $product->price = $request->input("price");
        $product->save();

        return redirect("products")->with([
            "status" => "Product was updated successfully."
        ]);
    }

    /**
     * Deletes the product
     * @param Product $product
     * @return redirect
     */
    public function destroy(Product $product) {
        $product->delete();
        return redirect('products')->with([
            'status' => 'Product deleted successfully.'
        ]);
    }

    private function uploadImage($img) {
        $imgName = str_random(3) . '_' . time() . '.' . $img->getClientOriginalExtension();
        $destinationPath = public_path('/images');
        $img->move($destinationPath, $imgName);

        return $imgName;
    }
}