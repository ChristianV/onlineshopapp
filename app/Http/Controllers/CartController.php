<?php

namespace App\Http\Controllers;

use App\Http\Requests\CartRequest;
use App\Mail\CheckoutOrder;
use App\Product;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller {

    /**
     * List of products from cart
     * param CartRequest $request
     * @return view
     */
    public function index(CartRequest $request) {
        return view("cart.index")->with([
            "products" => session()->get("cart")
        ]);
    }

    /**
     * Add product to cart
     * @param CartRequest $request
     * @param Product $product
     * @return redirect
     */
    public function add(CartRequest $request, Product $product) {
        $cartProduct = session()->get("cart." . $product->id);
        if($cartProduct) {
            $getQuantity = session()->get("cart." . $product->id . ".quantity");
            session()->put("cart." . $product->id . ".quantity", $getQuantity + 1);
        } else {
            session()->push("cart." . $product->id, $product);
            session()->put("cart." . $product->id . ".quantity", 1);
        }

        return redirect("products")
            ->with([
                "status" => "You added product to cart successfully."
            ]);
    }

    /**
     * Remove product to cart
     * @param CartRequest $request
     * @param Product $product
     * @return redirect
     */
    public function remove(CartRequest $request, Product $product) {
        session()->pull("cart." . $product->id, $product);

        return redirect("cart")
            ->with([
                "status" => "You removed product from cart successfully."
            ]);
    }

    /**
     * Checkout cart
     * @param CartRequest $request
     * @return redirect
     */
    public function checkout(CartRequest $request) {
        $validator = Validator::make($request->all(), [
            "name" => "required",
            "contactDetails" => "required"
        ]);

        if($validator->fails()) {
            return back()->withInput()->withErrors($validator->errors());
        }

        $contactData = [
            "name" => $request->input("name"),
            "contactDetails" => $request->input("contactDetails"),
            "comments" => $request->input("comments")
        ];
        try {
            Mail::to(config("shopVariables.mailTo"))
                ->send(new CheckoutOrder(session()->get("cart"), $contactData));
        } catch(\Exception $e) {
            return redirect("cart")
                ->with([
                    "status" => "500 Internal Server Error"
                ]);
        }

        session()->pull("cart");

        return redirect("cart")
            ->with([
                "status" => "You checked out successfully. Items from cart were removed."
            ]);
    }
}